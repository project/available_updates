<?php

namespace Drupal\Tests\available_updates\Functional;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Url;
use Drupal\Tests\basic_auth\Traits\BasicAuthTestTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Test available updates response.
 *
 * @group available_updates
 */
class AvailableUpdatesTest extends BrowserTestBase {

  use BasicAuthTestTrait;

  use UserCreationTrait;

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  public static $modules = [
    'user',
    'update',
    'basic_auth',
    'available_updates',
  ];

  /**
   * Tests the setting form.
   */
  public function testAvailableUpdatesResponse() {
    $this->drupalGet('/drupal/available-updates');
    $this->assertResponse(401);
    try {
      $account = $this->createUser(['access available updates'], 'user_test');
      $url = Url::fromRoute('available_updates.updates');
      $this->basicAuthGet($url, $account->getAccountName(), $account->pass_raw);
      $this->assertResponse('200');
    }
    catch (EntityStorageException $exception) {
      // Log.
    }

  }

}
