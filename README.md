CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 


INTRODUCTION
------------

Current Maintainer: Nasser Tijani <nassertijani@gmail.com>

This module is simply about exposing available updates in a consumable format
by other clients (JSON).

The result of this module can be explored for a display in a fontend that 
exposes for example a dashborad displaying a set of Drupal projects.

Just go to the following endpoint : /drupal/available-updates to see all 
the updates available in your Drupal.

To access these updates (Drupal core, modules, themes), all you need is a
basic authentication with user who has the permission access available 
updates.

REQUIREMENTS
------------

Update and Basic authentication modules (Drupal core modules).

INSTALLATION
------------

```
$ composer require 'drupal/available_updates:^1.1'
```

CONFIGURATION
-------------

* Add a role that can access the available drupal updates
* Assign permission ``` access available updates ```  to the role just created
* Access ``` /drupal/available-updates ``` with basic authentication
