<?php

namespace Drupal\available_updates\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\update\UpdateManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AvailableUpdatesController.
 */
class AvailableUpdatesController extends ControllerBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new AvailableUpdatesController object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * Get the available updates.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return available updates.
   */
  public function getAvailableUpdates(): JsonResponse {

    $this->moduleHandler->loadInclude('update', 'inc', 'update.manager');
    $this->moduleHandler->loadInclude('update', 'inc', 'update.compare');

    $calculated_updates = [];
    if ($available = update_get_available(TRUE)) {

      $calculated_updates = update_calculate_project_data($available);

      foreach ($calculated_updates as $project_name => $project_details) {
        if ($project_details['status'] == UpdateManager::CURRENT) {
          unset($calculated_updates[$project_name]);
        }
      }
    }
    return new JsonResponse([
      $this->config('system.site')->get('name') => $calculated_updates,
    ]);
  }

}
